'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import {IndexRoute, Route, Router} from 'react-router';
import '../css/app.scss';
import createBrowserHistory from 'history/lib/createBrowserHistory';

import Body from './components/body.jsx';
import Home from './components/home.jsx';
import ArtList from './components/artist/artlist.jsx';
import ArtDetail from './components/art/art-detail.jsx';
import ArtistBio from './components/artist/artistbio.jsx';

class App extends React.Component {
  render() {
    return (
      <Router history={createBrowserHistory()}>
        <Route path="/" component={Body}>
           <Route path="artlist" component={ArtList}/>
          <Route path="art/:art_id" component={ArtDetail} />
          <Route path="artist/:artistID" component={ArtistBio}/>
        </Route>
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));

var Firebase = require('firebase');
var myRootRef = new Firebase('https://bnsf-experience.firebaseio.com/');

