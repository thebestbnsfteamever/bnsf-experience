'use strict';

import React from 'react';
import {
  Col,
  Grid,
  Jumbotron,
  Row
} from 'react-bootstrap';

export default class Home extends React.Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col md={12}>
            <Jumbotron>
              <h1>Welcome!</h1>
              <p>
                BNSF is a company with a rich culture.  This app is your portal into the depths of the BNSF Experience.
              </p>
            </Jumbotron>
          </Col>
        </Row>
      </Grid>
    );
  }
}
