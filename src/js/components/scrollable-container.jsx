import React from 'react';

export default class ScrollableContainer extends React.Component {
  render() {
    let style = {
      overflowY: 'auto',
      overflowX: 'none'
    };

    return (
      <div style={style}>
        {this.props.children}
      </div>
    );
  }
}
