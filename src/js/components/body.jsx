'use strict';

import React from 'react';
import AppBar from './header/app-bar.jsx';

export default class Body extends React.Component {
  render() {
    return (
      <div>
        <AppBar />
        {this.props.children}
      </div>
    )
  }
}
