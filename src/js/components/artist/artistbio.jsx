'use strict';

import React from 'react';
import {
  Col,
  Grid,
  Image,
  Row
} from 'react-bootstrap';

import Works from './artlist.jsx';

export default class ArtistBio extends React.Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col xs={6} md={4}>
            <Image src="/images/thumbnaildiv.png" thumbnail />
            <h3>Artist Name</h3>
            <p>This artist is a master of their craft....</p>
          </Col>        
        </Row>
        <Row>
          <Works/>
        </Row>
      </Grid>
    );
  }
}
