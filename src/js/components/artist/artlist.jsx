'use strict';

import React from 'react';
import Firebase from 'firebase';

import {PageHeader, Grid} from 'react-bootstrap';

export default class ArtList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        artist: {   name: "Google"},
        paintings: []
    };
  }


  componentDidMount() {
                   this.firebaseRef = new Firebase("https://bnsf-experience.firebaseio.com/artwork/paintings/");
                   this.firebaseRef.on("child_added", function(dataSnapshot) {
                       this.paintings = [];
                       this.paintings.push(dataSnapshot.key());
                       this.setState({
                             paintings: this.paintings
                          });
                 }.bind(this));
             }

  render() {
      let paintings = this.state.paintings.map((painting) => {
          return (
              <li>{painting}</li>
              )
      });

    return (
      <Grid>
        <PageHeader>Artwork by {this.state.artist.name}</PageHeader>
        <ul>
          {paintings}
        </ul>
      </Grid>
    )

    }
  }
