import React from 'react';
import {
  Thumbnail
} from 'react-bootstrap';

export default class ArtImage extends React.Component {
  render() {
    return (
      <div className='art-image'>
        <Thumbnail
          src='http://rjmorrisseyart.com/wp-content/uploads/2013/12/Arts-logo.png'
          onClick={this.props.onClick}/>
      </div>
    );
  }
}
