import React from 'react';
import ScrollableContainer from '../scrollable-container.jsx';
import {Well} from 'react-bootstrap';
export default class ArtDesc extends React.Component {
    render() {
        return (
          <Well>
            <ScrollableContainer>
              {this.props.description}
            </ScrollableContainer>
          </Well>
        );
      }
}
