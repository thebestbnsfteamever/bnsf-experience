import React from 'react';
import ScrollableContainer from '../scrollable-container.jsx';
import {
  Link
} from 'react-router';

export default class ArtInfo extends React.Component {
    render() {
        return (
            <div>
                <h2>{this.props.name}</h2>
                <h4>
                  <Link to={'/artist/' + this.props.artist.id}>
                    {this.props.artist.name}
                  </Link>
                </h4>
            </div>

        );


    }
}
