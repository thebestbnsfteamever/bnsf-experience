import React from 'react';
import ArtImage from './art-image.jsx';
import ArtDesc from './art-desc.jsx';
import ArtInfo from './art-info.jsx';
import {
    Col,
    Grid,
    Row
} from 'react-bootstrap';

export default class ArtDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showFullScreen: false,
      art: {
        id: this.props.params.art_id,
        name: 'Art is cool',
        description: 'This is the artwork by the famous artist of the 19th century. This is an abstract painting.',
        artist: {
          name: 'Google',
          id: 1
        }
      }
    };

    this.onThumbnailClick = this.onThumbnailClick.bind(this);
  }

  render() {
    let content = this.renderContent(this.state.showFullScreen);
    return (
      <Grid>
        <Row>
          <Col xs={12}>
            <ArtImage {...this.state.art} onClick={this.onThumbnailClick} />
          </Col>
        </Row>
        {content}
      </Grid>
    );
  }

  renderContent(showFullScreen) {
    if (!showFullScreen) {
      return (
        <div>
          <Row>
              <Col xs={12}>
                  <ArtInfo  {...this.state.art} />
              </Col>
          </Row>
          <Row>
              <Col xs={12}>
                  <ArtDesc  {...this.state.art} />
              </Col>
          </Row>
        </div>
      );
    }
  }

  onThumbnailClick() {
    console.log(new Date(), 'clicked!');
  }
}
