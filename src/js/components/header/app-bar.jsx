import React from 'react';
import {
  Navbar,
  NavBrand
} from 'react-bootstrap';
import { IndexLink } from 'react-router';

export default class AppBar extends React.Component {
  render() {
    return (
      <Navbar>
        <NavBrand>
          <IndexLink to="/" activeClassName="active">BNSF Experience</IndexLink>
        </NavBrand>
      </Navbar>
    );
  }
};
